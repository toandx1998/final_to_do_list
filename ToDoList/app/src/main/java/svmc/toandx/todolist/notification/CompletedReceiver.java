package svmc.toandx.todolist.notification;

        import android.content.BroadcastReceiver;
        import android.content.Context;
        import android.content.Intent;
        import android.widget.Toast;

        import androidx.core.app.NotificationManagerCompat;

        import svmc.toandx.todolist.activity.MainActivity;
        import svmc.toandx.todolist.database.CustomDataBase;
        import svmc.toandx.todolist.model.Task;


/*
        Tao button Completed trong Notifcation
 */
public class CompletedReceiver extends BroadcastReceiver {

    CustomDataBase customDataBase;
    @Override
    public void onReceive(Context context, Intent intent) {
        int id = Integer.parseInt( intent.getStringExtra("ID")  );
        NotificationManagerCompat notificationManagerCompat = NotificationManagerCompat.from(context.getApplicationContext());
        notificationManagerCompat.cancel(id);
        customDataBase=CustomDataBase.getInstance(context);
        Task updateTask = customDataBase.getTask(id);
        // Update Task completed
        if (updateTask!=null)
        {
            updateTask.status=1-updateTask.status;
            customDataBase.updateTask(id,updateTask);
        }
        Toast.makeText(context, "The task is completed" , Toast.LENGTH_SHORT).show();
    }
}
